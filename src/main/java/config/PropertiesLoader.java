package config;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import static java.util.Objects.isNull;
import static utils.Constant.PropetiesLoader.*;

public class PropertiesLoader {
    private PropertiesLoader() {
    }

    public static HashMap<String, Object> get() {

        Yaml yaml = new Yaml();

        String configurationFile = String.format(ARQUIVO_CONFIGURACAO_POR_AMBIENTE, getEnv());

        if (configurationFile != null) {
            try (InputStream in = Files.newInputStream(getResourcePath(configurationFile))) {
                return yaml.load(in);
            } catch (IOException | IllegalAccessException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return new HashMap<>();
    }

    private static Path getResourcePath(String configurationFile) throws IllegalAccessException {
        try {
            URL urlResource = ClassLoader.getSystemResource(configurationFile);
            if (isNull(urlResource)) {
                lancarExcecaoArquivoConfiguracaoNaoEncontrado(configurationFile);
            }
            return Paths.get(urlResource.toURI());
        } catch (URISyntaxException e) {
            lancarExcecaoArquivoConfiguracaoNaoEncontrado(configurationFile);
            return null;
        }
    }

    public static String getEnv() {
        return System.getProperty("env", ENV_PADRAO);
    }

    private static void lancarExcecaoArquivoConfiguracaoNaoEncontrado(String configurationFile) throws IllegalAccessException {
        throw new IllegalAccessException(String.format(MENSAGEM_LOGGER_ERRO_ARQUIVO_CONFIGURACAO, configurationFile));
    }
}