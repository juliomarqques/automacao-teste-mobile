package screen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import core.BaseScreen;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ListScreen extends BaseScreen {

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"codigo\"]")
    private MobileElement inputStudentCode;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"aluno\"]")
    private MobileElement inputStudentName;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Salvar\"]")
    private MobileElement btnToSave;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Cancelar\"]")
    private MobileElement btnCancel;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"logout\"]")
    private MobileElement btnLogout;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"search\"]")
    private MobileElement inputSearch;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"11112 - Pedro Raniel\"]")
    private MobileElement searchReturn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"haveCode\"]")
    private MobileElement messageAlert;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Dados salvos!\"]")
    private MobileElement messageSuccess;

    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
            ".scrollIntoView(new UiSelector().text(\"11112 - Pedro Raniel\"))")
    private MobileElement studentNameOnList;


    public ListScreen(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    public ListScreen txtStudentCode(String studentCode) {
        this.inputStudentCode.sendKeys(studentCode);
        return this;
    }

    public ListScreen txtStudentName(String studentName) {
        this.inputStudentName.sendKeys(studentName);
        return this;
    }

    public ListScreen clickBtntoSave() {
        this.btnToSave.click();
        return this;
    }

    public ListScreen clickBtnCancel() {
        this.btnCancel.click();
        return this;
    }

    public LoginScreen clickBtnLogout() {
        this.btnLogout.click();
        return new LoginScreen(driver);
    }

    public void isVisivelBtnLogout() {
        assertTrue(wait.until(ExpectedConditions.visibilityOf(this.btnLogout)).isDisplayed());
    }

    public ListScreen researchStudent(String name) {
        this.inputSearch.sendKeys(name);
        return this;
    }

    public String searchStudentNameInList() {
        return this.studentNameOnList.getText();
    }

    public String getSearchReturn() {return this.searchReturn.getText();}

    public String getMessageAlert() {
        return this.messageAlert.getText();
    }

    public String getMessageSuccess() {
        return this.messageSuccess.getText();
    }

}
