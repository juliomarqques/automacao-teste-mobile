package screen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import core.BaseScreen;

public class LoginScreen extends BaseScreen {

    @AndroidFindBy(xpath = "//android.widget.EditText[@text=\"E-mail\"]")
    private MobileElement inputEmail;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text=\"Senha\"]")
    private MobileElement inputPassword;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"entrar\"]")
    private MobileElement btnEnter;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Erro no login!\"]")
    private MobileElement messageError;

    public LoginScreen(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    public LoginScreen txtEmail(String email) {
        this.inputEmail.sendKeys(email);
        return this;
    }

    public LoginScreen txtPassword(String password) {
        this.inputPassword.sendKeys(password);
        return this;
    }

    public ListScreen btnEnter() {
        this.btnEnter.click();
        return new ListScreen(driver);
    }

    public LoginScreen btnEnterError() {
        this.btnEnter.click();
        return this;
    }

    public String getMessageError() {
        return this.messageError.getText();
    }

}