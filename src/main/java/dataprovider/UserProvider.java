package dataprovider;

import dto.User;
import org.testng.annotations.DataProvider;

public class UserProvider {

    @DataProvider(name = "getUser")
    public static Object[][] getUser() {
        User user = new User("teste@teste.com", "123456");
        return new Object[][]{
                {user}
        };
    }

    @DataProvider(name = "getInvalidEmail")
    public static Object[][] invalidEmail() {
        User user = new User("testeerror@teste.com", "123456");
        return new Object[][]{
                {user}
        };
    }

    @DataProvider(name = "getInvalidPassword")
    public static Object[][] invalidPassword() {
        User user = new User("teste@teste.com", "123");
        return new Object[][]{
                {user}
        };
    }

    @DataProvider(name = "getEmptyEmail")
    public static Object[][] EmptyEmail() {
        User user = new User("", "123456");
        return new Object[][]{
                {user}
        };
    }

    @DataProvider(name = "getEmptyPassword")
    public static Object[][] EmptyPassword() {
        User user = new User("teste@teste.com", "");
        return new Object[][]{
                {user}
        };
    }

    @DataProvider(name = "getEmptyEmailAndPassword")
    public static Object[][] EmptyEmailAndPassword() {
        User user = new User("", "");
        return new Object[][]{
                {user}
        };
    }
}
