package business;

import core.BaseScreen;
import dto.User;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class RegisterRules extends BaseScreen {
    private LoginRules login;

    public RegisterRules(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    public String registerStudent(User user) {
        login = new LoginRules(driver);

        return login.doLogin(user)
                .txtStudentCode("101210")
                .txtStudentName("Luiz Santos")
                .clickBtntoSave()
                .getMessageSuccess();

    }

    public String registerExistingStudent(User user) {
        login = new LoginRules(driver);

        return login.doLogin(user)
                .txtStudentCode("12323")
                .txtStudentName("Eduardo Finotti")
                .clickBtntoSave()
                .getMessageAlert();

    }

    public String searchStudentBySearchField(User user) {
        login = new LoginRules(driver);

        return login.doLogin(user)
                .researchStudent("Pedro Raniel")
                .getSearchReturn();

    }

    public String searchStudentByScroll(User user) {
        login = new LoginRules(driver);

        return login.doLogin(user)
                .searchStudentNameInList();

    }
}