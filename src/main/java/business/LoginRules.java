package business;

import core.BaseScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import dto.User;
import screen.ListScreen;
import screen.LoginScreen;

public class LoginRules extends BaseScreen {
    protected LoginScreen login;

    public LoginRules(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    public ListScreen doLogin(User user) {
        login = new LoginScreen(driver);

        login.txtEmail(user.getEmail())
                .txtPassword(user.getPassword())
                .btnEnter()
                .isVisivelBtnLogout();

        return new ListScreen(driver);
    }

    public String loginWithError(User user) {
        login = new LoginScreen(driver);

        return login.btnEnterError()
                .getMessageError();
    }

}