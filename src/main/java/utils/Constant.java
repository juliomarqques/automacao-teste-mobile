package utils;

public class Constant {

    private Constant() { }

    public static class Messages {
        public static final String LOGIN_ERROR_MESSAGE = "Erro no login!";
        public static final String MESSAGE_DATA_SAVED = "Dados salvos!";
        public static final String EXISTING_STUDENT_MESSAGE = "Já existe um aluno com este código!";
        public static final String STUDENT_NAME_TO_SEARCH = "11112 - Pedro Raniel";
    }

    public static class PropetiesLoader {
        public static final String ENV_PADRAO = "bs";
        public static final String ARQUIVO_CONFIGURACAO_POR_AMBIENTE = "configuration-%s.yml";
        public static final String MENSAGEM_LOGGER_ERRO_ARQUIVO_CONFIGURACAO = "Não foi possível carregar o arquivo de configuração: %s";
    }

}