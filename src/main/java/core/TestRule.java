package core;

import config.PropertiesLoader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import static io.appium.java_client.remote.AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS;
import static io.appium.java_client.remote.MobileCapabilityType.APP;
import static io.appium.java_client.remote.MobileCapabilityType.DEVICE_NAME;
import static org.openqa.selenium.remote.CapabilityType.PLATFORM_NAME;

public class TestRule {

    protected static AppiumDriver<MobileElement> driver;
    protected String NAME_DEVICE;
    protected String PATH_APP;
    protected String APPIUM_URL;

    @BeforeClass
    public void setupSession() throws MalformedURLException {
        HashMap<String, Object> properties = PropertiesLoader.get();

        NAME_DEVICE = String.valueOf(properties.get("device.name"));
        PATH_APP = String.valueOf(properties.get("path.app"));
        APPIUM_URL = String.valueOf(properties.get("appium.url"));

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(PLATFORM_NAME, MobilePlatform.ANDROID);
        capabilities.setCapability(DEVICE_NAME, NAME_DEVICE);
        capabilities.setCapability(AUTO_GRANT_PERMISSIONS, true);
        capabilities.setCapability(APP, PATH_APP);

        driver = new AppiumDriver<>(new URL(APPIUM_URL), capabilities);
    }

    @AfterMethod
    public void resetApp() {
        driver.resetApp();
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

}