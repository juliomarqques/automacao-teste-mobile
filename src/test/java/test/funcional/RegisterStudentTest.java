package test.funcional;

import business.RegisterRules;
import core.TestRule;
import dataprovider.UserProvider;
import dto.User;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static utils.Constant.Messages.*;

public class RegisterStudentTest extends TestRule {
    protected RegisterRules register;

    @Test(dataProvider = "getUser", dataProviderClass = UserProvider.class)
    public void deveriaCadastrarAlunoComSucesso(User user) {
        register = new RegisterRules(driver);

        String message = register.registerStudent(user);
        assertEquals(MESSAGE_DATA_SAVED, message);
    }

    @Test(dataProvider = "getUser", dataProviderClass = UserProvider.class)
    public void naoDeveriaCadastrarAlunoExistente(User user) {
        register = new RegisterRules(driver);

        String message = register.registerExistingStudent(user);
        assertEquals(EXISTING_STUDENT_MESSAGE, message);
    }

    @Test(dataProvider = "getUser", dataProviderClass = UserProvider.class)
    public void deveriaBuscarAlunoPeloCampoSearch(User user) {
        register = new RegisterRules(driver);

        String message = register.searchStudentBySearchField(user);
        assertEquals(STUDENT_NAME_TO_SEARCH, message);
    }

    @Test(dataProvider = "getUser", dataProviderClass = UserProvider.class)
    public void deveriaBuscarAlunoPeloLista(User user) {
        register = new RegisterRules(driver);

        String message = register.searchStudentByScroll(user);
        assertEquals(STUDENT_NAME_TO_SEARCH, message);
    }
}