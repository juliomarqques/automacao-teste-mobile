package test.funcional;

import business.LoginRules;
import dataprovider.UserProvider;
import dto.User;
import org.testng.annotations.Test;
import core.TestRule;

import static org.testng.Assert.assertEquals;
import static utils.Constant.Messages.LOGIN_ERROR_MESSAGE;

public class LoginTest extends TestRule {
    protected LoginRules login;

    @Test(dataProvider = "getUser", dataProviderClass = UserProvider.class)
    public void deveriaRealizarLoginComSucesso(User user) {
        login = new LoginRules(driver);

        login.doLogin(user);
    }

    @Test(dataProvider = "getInvalidEmail", dataProviderClass = UserProvider.class)
    public void naoDeveriaRealizarLoginEmailInvalido(User user) {
        login = new LoginRules(driver);

        String mesageError = login.loginWithError(user);
        assertEquals(LOGIN_ERROR_MESSAGE, mesageError);
    }

    @Test(dataProvider = "getInvalidPassword", dataProviderClass = UserProvider.class)
    public void naoDeveriaRealizarLoginSenhaInvalido(User user) {
        login = new LoginRules(driver);

        String messageError = login.loginWithError(user);
        assertEquals(LOGIN_ERROR_MESSAGE, messageError);
    }

    @Test(dataProvider = "getEmptyEmail", dataProviderClass = UserProvider.class)
    public void naoDeveriaRealizarLoginComCampoEmailVazio(User user) {
        login = new LoginRules(driver);

        String messageError = login.loginWithError(user);
        assertEquals(LOGIN_ERROR_MESSAGE, messageError);
    }

    @Test(dataProvider = "getEmptyPassword", dataProviderClass = UserProvider.class)
    public void naoDeveriaRealizarLoginComCampoSenhaVazio(User user) {
        login = new LoginRules(driver);

        String messageError = login.loginWithError(user);
        assertEquals(LOGIN_ERROR_MESSAGE, messageError);
    }

    @Test(dataProvider = "getEmptyEmailAndPassword", dataProviderClass = UserProvider.class)
    public void naoDeveriaRealizarLoginComCampoEmailESenhaVazio(User user) {
        login = new LoginRules(driver);

        String messageError = login.loginWithError(user);
        assertEquals(LOGIN_ERROR_MESSAGE, messageError);
    }

}