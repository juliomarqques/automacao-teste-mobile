#!/bin/bash

curl -X POST \ -H "Content-Type:application/json; charset=utf-8" --data "{
    \"channel\": \"#general\", \"username\": \"Bot-Gitlab\", \"icon_emoji\": \":icon-gitlab:\", \"text\": \"*FALHA - Execução dos Testes*\",
    \"attachments\": [
        {
            \"mrkdwn_in\": [\"text\"],
            \"color\": \"#d00000\",
            \"author_name\": \"$CI_COMMIT_AUTHOR\",
            \"text\": \"*JOB <$CI_JOB_URL|${CI_JOB_ID}> Rodou com falha na Pipeline <$CI_PIPELINE_URL|${CI_PIPELINE_ID}>*\",
            \"fields\": [
                {
                    \"title\": \"Trigger source\",
                    \"value\": \"$CI_PIPELINE_SOURCE\",
                     \"short\": true
                },
                {
                    \"title\": \"Branch\",
                    \"value\": \"<$CI_PROJECT_URL/-/tree/$CI_COMMIT_BRANCH|${CI_COMMIT_BRANCH}>\",
                    \"short\": true
                },
                {
                    \"title\": \"Commit message\",
                    \"value\": \"<$CI_PROJECT_URL/-/commit/$CI_COMMIT_SHA|${CI_COMMIT_TITLE}>\",
                    \"short\": true
                }
            ],
            \"footer\": \"<$CI_PROJECT_URL>\",
            \"footer_icon\": \"https://www.stickpng.com/assets/images/5847f997cef1014c0b5e48c1.png\",
        }
    ]
}" $SLACK_WEBHOOK_URL