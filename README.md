# Projeto de automação de testes mobile

## Descrição
O respectivo projeto é responsável pela automação dos testes funcionais de UI, para a funcionalidade de Login e Cadastro, no aplicativo Qazando para Smartphone Android.


## Pré-requisitos
### Ferramentas/Tecnologias:

- [Java JDK8](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) - Como tecnologia (linguagem de programação)
- [TestNG](https://testng.org/) - Como _TestRunner_
- [Maven](https://maven.apache.org/) - Como gerenciador de dependências
- [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) - Como IDE
- [Appium](https://github.com/appium/appium-desktop/releases/tag/v1.22.3-4) - Como biblioteca para automação de testes
- [Android studio](https://developer.android.com/studio) - Como emulador Android

### Instalação

Para instalar o projeto faça o clone:
```
git clone https://gitlab.com/juliomarqques/automacao-teste-mobile.git
```

### Organização das pastas do projeto
Árvore de arquivos :
```
├── script
├── src
│   ├── main
│   │   ├── java
│   │   │   ├── business
│   │   │   ├── config
│   │   │   ├── core
│   │   │   ├── dataprovider
│   │   │   ├── dto
│   │   │   ├── screen
│   │   │   ├── utils
│   │   ├── resources
│   │   │   └── app
│   ├── test
│   │   ├── java
│   │   │   └── test
│   │   │       └── funcional
│   │   └── resources
````

### Configurando dispositivos
### Criando dispositivo virtual Android (emulador) a partir do Android Studio:

1) Abra o Android Studio.
2) Clique em Ferramentas -> AVD Manager -> Criar dispositivo virtual -> Selecione o dispositivo e a versão do sistema operacional -> Concluir.
3) Depois que o dispositivo virtual for criado, clique em Iniciar este AVD no emulador.
4) Comando para visualizar a lista de dispositivos conectados `adb devices`

###  Configuração do dispositivo real Android:

1) Conecte o dispositivo Android real à máquina (Desktop/Laptop)
2) Ative as opções do desenvolvedor no celular Android
3) Ative a depuração USB
4) Execute o comando `adb devices` no prompt do cmd para verificar se o dispositivo é reconhecido

#### Obs: Para execução dos testes locais tanto emulador quanto dispositivo real, a automação realizará a instalação do apk no dispositivo. Mas, antes é necessário informar o caminho do apk. No arquivo 'configuration-local.yml' na key 'path.app' onde está 'path' insira o caminho da raiz até a pasta do projeto, sem as chaves.

### Criando conta de demonstração no BrowserStack (Para rodar os testes em dispositivos na nuvem)
1) Acesse [BrowserStack](https://www.browserstack.com/)
2) Vá em 'Get a demo' e preencha os dados.
3) Para realizar o upload do App, faça login e vá para Dashboard, caso não esteja.
4) Na aba de navegação, vá para 'App Automate'.
5) Clique em 'UPLOAD APP' e procure o apk no projeto `automacao-teste-mobile -> src -> main -> resources`
6) Copie o token gerado
7) Abra o arquivo 'configuration-bs.yml', na key 'path.app' cole o token sem as chaves {}.
8) Ainda na tela do 'App Automate', clique em 'ACCESS KEY' copie suas credenciais. No arquivo 'configuration-bs.yml', key 'appium.url', cole-os sem as chaves.

### Iniciando dispositivos
### Inicie o emulador do Android na linha de comando

```
Comando para listar os dispositivos: `emulator -list-avds`
Comando para iniciar: `emulator -avd <avd_name>`
Comando para parar: `adb -e emu kill`
```
### Para usar o dispositivo real, deixe o dispositivo desbloqueado e o conecte via USB ao computador

## Execução dos testes
Na classe <b> Constant</b> a variável ENV_PADRAO é responsavel por determinar em qual ambiente os testes serão executados ex: 
* <b>local</b> : Executa os testes no emulador ou dispositivo real
* <b>bs</b>: Executa os testes no BrowserStack

#### Execução
* Comando para executar os testes com maven:
```
  mvn clean test
 ```
* Para execução manual, clique com botão direito do mouse no diretório 'funcional' e então selecione Run.

## Pipeline
Execução dos testes
![pipeline](https://gitlab.com/juliomarqques/automacao-teste-mobile/-/wikis/uploads/45d0abff8ff5ba45c2d9f2a94418fdbb/pipeline.jpg)

## Report (Allure reports)
Ao finalizar a execução dos testes, execute o comando no terminal do projeto para subir o servidor do Allure e em seguida o relatório vai abrir no browser:
```
  mvn allure:serve
 ```
![Allure](https://gitlab.com/juliomarqques/automacao-teste-mobile/-/wikis/uploads/aeffa7581c51a47ddeb264a03875237d/Allure.jpg)

## Integração com Slack
![slack](https://gitlab.com/juliomarqques/automacao-teste-mobile/-/wikis/uploads/547f0db794ce094d5c65704da9b437e3/slack.jpg)

###### Autor:
[**Júlio Marques**](https://www.linkedin.com/in/julio-marques/)